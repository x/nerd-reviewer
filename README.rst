NerdReviewer - automates dull daily routine code reviews
========================================================

Nerd Code Reviewer analyzes commits and tell authors where and why they
code won't pass human code review process.

Such automation optimizes work on open source projects and simplifies life
of new contributors, because they will get interactive step by step guide
how to make their patches attractive for project's maintainers.

.. image:: doc/source/images/american_gothic.jpg
   :alt: American Gothic
   :width: 650 px
