..
      Copyright 2015 Mirantis Inc. All Rights Reserved.

      Licensed under the Apache License, Version 2.0 (the "License"); you may
      not use this file except in compliance with the License. You may obtain
      a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
      WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
      License for the specific language governing permissions and limitations
      under the License.

NerdReviewer - automates dull daily routine code reviews
========================================================


Nerd Code Reviewer analyzes commits and tell authors where and why they
code won't pass human code review process.

Such automation optimizes work on open source projects and simplifies life
of new contributors, because they will get interactive step by step guide
how to make their patches attractive for project's maintainers.


.. image:: ./images/american_gothic.jpg
   :alt: American Gothic
   :width: 450 px
